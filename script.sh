#!/bin/bash

MOUNTED=false
MOUNTED_PATH="/path/to/fd"

trap "unmounting" SIGINT

main_menu(){
	echo "============================================"
	echo "=Selamat Datang pada program penghapus file="
	echo "============================================"
	mounting
	echo "Pilih angka dari 1-3 dibawah ini:"
	echo "1. Melihat seluruh file yang ada"
	echo "2. Menghapus suatu file"
	echo "3. Exit"
	echo -n "Masukkan angka: "
	read NUMBER
	if [[ $NUMBER -eq 1 ]]
	then
		look_all_files
	elif [[ $NUMBER -eq 2 ]]
	then
		delete_file
	else
		echo "Terima kasih telah berpartisipasi dengan Operating Santuy"
		unmounting
	fi
}

delete_file(){
	while [ 1 -gt 0 ]
	do
		printf "\n====Daftar File====\n"
		ls
		echo ""
		echo "Jika file yang dimaksud terdapat pada suatu folder, masukkan nama folder tersebut:"
		read FOLDER
		if [ "$FOLDER" = "-" ]
		then
			break
		else
			cd $FOLDER
		echo ""
		fi
	done
	echo "Masukkan nama file yang ingin anda hapus:"
	read FILE
	if [[ -f "$FILE" ]]
	then
		echo "File $FILE terhapus"
		rm "$FILE"
	else
		echo "File tidak ada"
	fi
	cd /mnt
	main_menu
}

mounting(){
	DIR=$(sudo fdisk -l | grep FAT32 | cut -c1-9)
	MOUNTED_PATH=$DIR
	IS_EXIST=`echo $DIR | wc -l`
	if [[ $IS_EXIST -eq 1 && "$MOUNTED" = false ]]
	then
		MOUNTED=true
		sudo mount $DIR /mnt
		cd /mnt
	elif [[ $IS_EXIST -eq 0 ]]
	then
		echo "Flash disk yang ingin diobservasi belum dipasang."
		echo "Notes: TOLONG DIJALANKAN ULANG PROGRAMNYA!"
		exit
	fi
}

unmounting() {
	echo "Flash drive telah di unmount!!"
	cd $HOME
	sudo umount $MOUNTED_PATH
	exit
}

look_all_files(){
	ls -R | grep "./" | sed -e "s/[^\/]*\//•► ••/g" -e "s/► •••/| ►/g" -e "s/►|/|/g " -e "s/•|/|/g" -e "s/►► ••/► •••/g" -e "s/•► ••/► •••/g" -e "s/:$//g"
	main_menu
}

main_menu
