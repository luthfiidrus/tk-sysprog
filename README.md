# Tugas Kelompok System Programming - Operating Santuy

## Sistem
* Versi Kernel : 4.15.1

## Cara menggunakan program kami
1. clone repository kami
2. Install custom kernel
3. Copy file-file yang diperlukan ke /home/user, berikut adalah daftar file-nya:
	- installer.sh
	- unused_service
	- script.sh
4. Menjalankan file installer.sh dengan perintah “sudo bash installer.sh”.
5. Restart sistem.
6. Program dapat digunakan.

