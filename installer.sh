#!/bin/bash

echo "Removing unused service...."
cat unused_service | while read line;
	do systemctl disable $line;
done
echo "Unused service removed!!"
echo "Preparing to install driver!!"
chmod +x script.sh
cp script.sh ./.script.sh
chmod +x ./.script.sh
SCRIPT_CHECK=$(cat .bashrc | grep script.sh | wc -l)
if [ $SCRIPT_CHECK -eq 0 ]
then
	echo "Installing now...."
	sudo echo "sudo bash ~/.script.sh" >> .bashrc
else
	echo "Driver already installed"
fi
echo "Finished"
source .bashrc
